L_OFFICE := libreoffice
GIT := git
EXIFTOOL := exiftool

# Check if the needed executable exist
EXECUTABLES = $(L_OFFICE) $(GIT) $(EXIFTOOL)
K := $(foreach exec,$(EXECUTABLES),\
	$(if $(shell command -v $(exec)),some string,$(error "No $(exec) in PATH")))

GIT_TAG := $(shell git describe --always --dirty)

TAG := $(GIT_TAG)

F_NAME := resume
IF_NAME := $(F_NAME).odt
OF_NAME := brodie-kurczynski-$(F_NAME)

.PHONY: export-pdf
export-pdf:
	# Using the '--convert-to pdf' option to create a PDF degrades the icon resolution for some reason
	$(L_OFFICE) --print-to-file --headless $(IF_NAME)
	$(EXIFTOOL) $(F_NAME).pdf -Version=$(TAG) -Title='Brodie Kurczynski resume'
	mv $(F_NAME).pdf $(OF_NAME).pdf
