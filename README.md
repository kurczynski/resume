# resume
My resume. I used the LibreOffice versioning for a while and it wasn't bad, but
this feels more natural as a developer.

## Export PDF
```
make export-pdf
```
